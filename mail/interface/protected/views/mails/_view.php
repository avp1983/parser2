<?php
/* @var $this MailsController */
/* @var $data Mails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mes_id')); ?>:</b>
	<?php echo CHtml::encode($data->mes_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from')); ?>:</b>
	<?php echo CHtml::encode($data->from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('to')); ?>:</b>
	<?php echo CHtml::encode($data->to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_unix')); ?>:</b>
	<?php echo CHtml::encode($data->date_unix); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_text')); ?>:</b>
	<?php echo CHtml::encode($data->date_text); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('subject')); ?>:</b>
	<?php echo CHtml::encode($data->subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body')); ?>:</b>
	<?php echo CHtml::encode($data->body); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('attachments')); ?>:</b>
	<?php echo CHtml::encode($data->attachments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msg_num')); ?>:</b>
	<?php echo CHtml::encode($data->msg_num); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('html')); ?>:</b>
	<?php echo CHtml::encode($data->html); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::encode($data->uid); ?>
	<br />

	*/ ?>

</div>