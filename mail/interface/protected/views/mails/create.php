<?php
/* @var $this MailsController */
/* @var $model Mails */

$this->breadcrumbs=array(
	'Mails'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Mails', 'url'=>array('index')),
	array('label'=>'Manage Mails', 'url'=>array('admin')),
);
?>

<h1>Create Mails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>