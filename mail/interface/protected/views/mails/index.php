<?php
/* @var $this MailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mails',
);

$this->menu=array(
	array('label'=>'Create Mails', 'url'=>array('create')),
	array('label'=>'Manage Mails', 'url'=>array('admin')),
);
?>

<h1>Mails</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
