<?php
/* @var $this MailsController */
/* @var $model Mails */

$this->breadcrumbs=array(
	'Mails'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Mails', 'url'=>array('index')),
	array('label'=>'Create Mails', 'url'=>array('create')),
	array('label'=>'Update Mails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Mails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mails', 'url'=>array('admin')),
);
?>

<h1>View Mails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'mes_id',
		'from',
		'name',
		'to',
		'date_unix',
		'date_text',
		'subject',
		'body',
		'attachments',
		'msg_num',
		'html',
		'uid',
	),
)); ?>
