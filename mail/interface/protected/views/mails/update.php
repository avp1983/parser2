<?php
/* @var $this MailsController */
/* @var $model Mails */

$this->breadcrumbs=array(
	'Mails'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mails', 'url'=>array('index')),
	array('label'=>'Create Mails', 'url'=>array('create')),
	array('label'=>'View Mails', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Mails', 'url'=>array('admin')),
);
?>

<h1>Update Mails <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>