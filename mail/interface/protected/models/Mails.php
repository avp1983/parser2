<?php

/**
 * This is the model class for table "mails".
 *
 * The followings are the available columns in table 'mails':
 * @property integer $id
 * @property string $mes_id
 * @property string $from
 * @property string $name
 * @property string $to
 * @property integer $date_unix
 * @property string $date_text
 * @property string $subject
 * @property string $body
 * @property string $attachments
 * @property integer $msg_num
 * @property string $html
 * @property string $uid
 */
class Mails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mails';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_unix, msg_num', 'numerical', 'integerOnly'=>true),
			array('mes_id, from, name, to, date_text, subject, uid', 'length', 'max'=>255),
			array('body, attachments, html', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mes_id, from, name, to, date_unix, date_text, subject, body, attachments, msg_num, html, uid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mes_id' => 'Mes',
			'from' => 'From',
			'name' => 'Name',
			'to' => 'To',
			'date_unix' => 'Date Unix',
			'date_text' => 'Date Text',
			'subject' => 'Subject',
			'body' => 'Body',
			'attachments' => 'Attachments',
			'msg_num' => 'Msg Num',
			'html' => 'Html',
			'uid' => 'Uid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mes_id',$this->mes_id,true);
		$criteria->compare('from',$this->from,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('to',$this->to,true);
		$criteria->compare('date_unix',$this->date_unix);
		$criteria->compare('date_text',$this->date_text,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('attachments',$this->attachments,true);
		$criteria->compare('msg_num',$this->msg_num);
		$criteria->compare('html',$this->html,true);
		$criteria->compare('uid',$this->uid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
