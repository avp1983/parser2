#!/usr/bin/python
# -*- coding: utf-8 -*-

# directory where to save attachments 
'''
attachments_dir = '/var/www/o-mo/data/www/o-mo.ru/data' 
'''
attachments_dir = './data' 
#DataBase settings
'''
db={
    'host':'localhost',
    'user':'mail',
    'passwd':'159753',
    'db':'mail',
    'charset':'utf8'    
    }
'''
db={
    'host':'localhost',
    'user':'mails',
    'passwd':'mails',
    'db':'mails',
    'charset':'utf8'    
    }


# If you don't want send answer letter (automatically), this var must be 0
send_answer=0


#After this time (in seconds), if the connection to imap server does not happen, the script will fail
imap_server_connection_timeout = 60

#After this time (in seconds), if the letter has not downloaded, the script will skip this letter
imap_msg_skip_timeout = 180

# After this time (in seconds), the script finish work
script_execution_timeout = 60*4



create_table_sql ='''
CREATE TABLE IF NOT EXISTS `%s` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mes_id` varchar(255) DEFAULT NULL,
  `from` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `date_unix` int(10) DEFAULT NULL,
  `date_text` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `attachments` text,
  `msg_num` int(10) DEFAULT NULL,
  `html` text,
  `uid` varchar(255) DEFAULT NULL,
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=%s;
'''

version = '0.1'