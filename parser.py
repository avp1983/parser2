#!/usr/bin/python
# -*- coding: utf-8 -*-
import  imaplib, os
from email.Header import decode_header
import email
from email.utils import parseaddr
from random import randint
import chardet
import uuid
import signal
import MySQLdb
import re
import warnings
from email.utils import parsedate
import time
#from smtplib import SMTP_SSL as SMTP       # this invokes the secure SMTP protocol (port 465, uses SSL)
from smtplib import SMTP                  # use this for standard SMTP protocol   (port 25, no encryption)
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import conf
import datetime
#import string
start_time = 0
def init():
    global start_time
    start_time = time.time()     
    detach_dir = conf.attachments_dir
    if not os.path.isdir(detach_dir) or not os.access(detach_dir, os.W_OK):
        raise Exception("%s is not exist or not writeble"%detach_dir)
        
def check_execution_timeout():
    global start_time
    if time.time() - start_time > conf.script_execution_timeout:
        raise Exception("Time is over (script_execution_timeout)")
         

class Db(object):            
    def __init__(self):
        self. __db = MySQLdb.connect(host=conf.db['host'], user=conf.db['user'], passwd=conf.db['passwd'], db=conf.db['db'], charset=conf.db['charset'])
        self.__cursor = self. __db.cursor()
        self.select_table()
        self.get_column_names(self.table)
    def close(self):
        self. __db.close()   
    
    
    
    def create_table(self, name, autoincrement):        
        sql = conf.create_table_sql%(name, autoincrement)
        self.__cursor.execute(sql)
    
    def get_last_id(self, table):
        sql = "SELECT MAX(`id`) from `%s`"%table
        self.__cursor.execute(sql)
        last_id =   self.__cursor.fetchone()  
        if last_id: return last_id[0]
        return 0
    def select_table(self):
        name = self.gen_table_name() # now
        if self.table_exists(name):
            self.table = name
        else:
            name_prev = "mail_"+ self.prev_month()
            if self.table_exists(name_prev): #if previous table exists
                #get last id
                last_id = self.get_last_id(name_prev)
                self.create_table(name, last_id+1)
                              
            else:
                self.create_table(name, 0)
            self.table = name    
             
    def prev_month(self, when = None): 
        """Return (previous month's start date, previous month's end date)."""
        if not when:
            # Default to today.
            when = datetime.datetime.today()
        # Find previous month: http://stackoverflow.com/a/9725093/564514
        # Find today.
        first = datetime.date(day=1, month=when.month, year=when.year)
        # Use that to find the first day of this month.
        prev_month_end = first - datetime.timedelta(days=1)
        prev_month_start = datetime.date(day=1, month= prev_month_end.month, year= prev_month_end.year)
        # Return previous month's start and end dates in YY-MM-DD format.
        return prev_month_start.strftime('%m%Y')          
        
    
    def table_exists(self, table):
        '''if table exist, return 1'''
        stmt = "SHOW TABLES LIKE '%s'"%table
        self.__cursor.execute(stmt)
        result = self.__cursor.fetchone()
        if result:
            return 1
        return 0
    
    def gen_table_name(self):
        now_date = datetime.date.today()
        return "mail_%02d%s"%(now_date.month, now_date.year);
        
    
    def parse_fields(self, rec):
        name, field_type, is_null = rec[0], rec[1], rec[2]
        m = re.search(r'^([^\(]+)', field_type)
        type0 = m.group(0)           
        m = re.search(r'\d+', field_type)
        if hasattr(m, 'group'):
            length = m.group(0)               
        else:
            length = None
        return (name, type0, length)    
    
    
    def get_column_names(self, table):
        sql = 'SHOW COLUMNS FROM %s;'%table
        self.__cursor.execute(sql)
        data = self.__cursor.fetchall()
        self.fields=[self.parse_fields(rec) for rec in data if rec[3]=='']                
        return  self.fields
    
    def select(self, table):
        sql = 'SELECT * FROM %s;'%table
        self.__cursor.execute(sql)
        #d= [{'user':user, 'pwd':pwd, 'imap':imap, 'smtp':smtp} for Id, user, pwd, imap, smtp in self.__cursor.fetchall()]
        d= self.__cursor.fetchall()
        return d
         
    def insert(self, my_mail):
        sql = "INSERT  INTO `%s` (%s) VALUES  (%s)"
        names = []
        s=[]
        v=[]
        for n, t, l in self.fields:
            names.append('`%s`'%n)
            s.append('%s')
            if n in my_mail.keys():
                v.append(my_mail[n])
                #Check type                
                if t=='int':
                    if not isinstance(my_mail[n], int):                    
                        warnings.warn("Field %s must be integer"%n)
                        #check length
                length_f=len(str(my_mail[n]))
                if l is not None and length_f > int(l):                       
                        warnings.warn("Field %s  is longer then %s symbols"%(n, l))
            else:
                warnings.warn("No value for field %s"%n)
                v.append('')
        sql = sql%(self.table, ', '.join(names), ', '.join(s))
        self.__cursor.execute(sql, v)        
        try:
                self. __db.commit()
        except:
                return 0;
        return 1;
        #print  sql      
 
class Parser(object):
    def __init__(self, user, pwd, imap_server, timeout=60):
        self.user=user
        self.pwd=pwd
        self.FetchError = None      
        signal.signal(signal.SIGALRM, self.handler_init)
        # connecting to the gmail imap server
        try:    
            signal.alarm(timeout)        
            self.m = imaplib.IMAP4_SSL(imap_server)        
            self.m.login(user,pwd) 
            self.m.select("INBOX") # here you a can choose a mail box like INBOX instead
            # use m.list() to get all the mailboxes
            self.resp, items = self.m.search(None, "(ALL)") # you could filter using the IMAP rules here (check http://www.example-code.com/csharp/imap-search-critera.asp)
            if items[0] is not None:
                self.items = items[0].split()
                print "Count %d"%len(self.items)
            else:
                self.items = None
                print "No new letters"
            signal.alarm(0)    
        except:
            self.items = None
            print "Connection or login error. User skiped."
                    

    def   handler_init(self, signum, frame):
        print "Server is busy! Time is out (imap_server_connection_timeout)"
        self.items = None
        
    
    def handler_skip(self, signum, frame):        
        self.FetchError= "Can not download msg %s "  

    
    
    def gen_rand_dir(self, emailid):
        rand_name =  "%s__%s"%(uuid.uuid4(), emailid)
        return os.path.join(conf.attachments_dir, rand_name)
    
    
    def write_to_file(self, filename, file_data, emailid):        
        directory = self.gen_rand_dir(emailid)
        if not os.path.exists(directory):
            os.makedirs(directory)
        else:
            raise Exception("Can not create folder")
        if not filename:
            filename = 'part-%03d%s' % ( randint(1,100000), 'bin')
        else:
            try:
                filename =  self.decode_field(filename);   
            except:
                print "Filename exception in msg %s"%emailid
            filename = filename.replace(' ', '_')
        att_path = os.path.join(directory, filename)
        if not os.path.isfile(att_path)  and file_data is not None:
            fp = open(att_path, 'wb')
            fp.write(file_data)
            fp.close()
        else:
            raise Exception("Can not save file")
        return  att_path       
    
    def parse_attachment(self, message_part, mess_id, emailid):        
        content_disposition = message_part.get("Content-Disposition", None)        
        filename = message_part.get_filename()
        file_data = message_part.get_payload(decode=True) 
        if content_disposition:
            dispositions = content_disposition.strip().split(";")
            if bool(content_disposition and dispositions[0].lower() == "attachment"):                               
                return self.write_to_file(filename, file_data, emailid)
        if filename: #avp1983@list.ru msg_num=154 BUG fix do not save attachment due fucking microsoft outlook express 6
            return self.write_to_file(filename, file_data, emailid)
        return None 

    def detect_encode(self, s):
        result = chardet.detect(s)
        enc = result['encoding']
        if enc:
            try:
                s = unicode(s , enc).encode('utf8','replace')
            except:
                print "encoding troubles"
        return s              
    
    
    def to_utf(self, s, enc):
        if enc:
            try:
                s = unicode(s , enc).encode('utf8','replace')
            except:
                s = self.detect_encode(s)    
        else:
            s = self.detect_encode(s)   
        return s         
    
    def decode_field(self, content):
        if content is not None:
            try:
                decodefrag = decode_header(content)
            except:
                print "decode field error in msg %s"%self.emailid
                return content
            subj_fragments = []
            for s , enc in decodefrag:
                s = self.to_utf(s, enc)
                subj_fragments.append(s)               
            return ''.join(subj_fragments)    
        return None

    def parse(self, content, emailid):
       
        self.emailid = emailid
        #p = EmailParser()
        msgobj = email.message_from_string(content)
       
        subject = self.decode_field(msgobj['Subject'])   
        from0 =  self.decode_field(parseaddr(msgobj.get('From'))[1])
        name = self.decode_field(parseaddr(msgobj.get('From'))[0])
        msg_id = self.decode_field(msgobj.get('Message-ID'));
        resp, uid = self.m.fetch(emailid, "UID") 
        if resp=='OK':
            uid=uid[0]
        else:
            uid=None    
        date = self.decode_field(msgobj.get('Date'))
        tt = parsedate(date)
        if tt is None : 
            date_unix =None
        else:    
            date_unix = int(time.mktime(tt))
        #attachments = []
        attachments ='';
        body = None
        html = None
        for part in msgobj.walk():
            attachment = self.parse_attachment(part, msg_id, emailid)
            if attachment:
                #attachments.append(attachment)
                attachments =attachments +attachment+"\n";
            elif part.get_content_type() == "text/plain":
                if body is None:
                    body = ""            
                enc = part.get_content_charset()
                if enc is None:
                    result = chardet.detect(part.get_payload(decode=True))
                    enc = result['encoding']
                    
                    #print "Charset Is %s\n"%charenc
                try:
                    body += unicode(part.get_payload(decode=True), enc, 'replace' ).encode('utf8','replace')
                except:
                    print "Unicode exception in msg %s"%emailid  
            elif part.get_content_type() == "text/html":
                if html is None:
                    html = ""
                enc = part.get_content_charset()
                if enc is None:
                    result = chardet.detect(part.get_payload(decode=True))
                    enc = result['encoding']
                try:
                    html += unicode(part.get_payload(decode=True),  enc, 'replace').encode('utf8','replace')
                except:
                    print "Unicode exception in msg %s"%emailid    
                
        return {
            'mes_id': msg_id,    
            'subject' : subject,
            'body' : body,
            'date_text':date,
            'date_unix':date_unix, 
            'html' : html,
            'from' : from0, 
            'name': name, 
            'to' : parseaddr(msgobj.get('To'))[1],
            'attachments': attachments,
            'uid':uid,
            'create_time':int(time.time())
        }
    def get_mails(self, db, timeout, response_text, response_subject, smtp):        
        if self.items is None: return 0
        if self.resp == 'OK':           
            for emailid in self.items:                
                check_execution_timeout()
                signal.signal(signal.SIGALRM, self.handler_skip)
                signal.alarm(timeout)
                resp, data = self.m.fetch(emailid, "(RFC822)") # fetching the mail, "`(RFC822)`" means "get the whole stuff", but you can ask for headers only, etc
                signal.alarm(0)
                if self.FetchError is not None:
                    print  self.FetchError%str(emailid)
                    self.FetchError = None            
                    continue
                print "msg %s"%emailid;
                if resp == 'OK':
                    email_body = data[0][1] # getting the mail content                  
                    
                    my_mail = self.parse(email_body, emailid)
                    
                    my_mail['msg_num'] = emailid
                    if db.insert(my_mail):                        
                        typ, data =self.m.store(emailid,'+FLAGS','\Seen')
                        if typ != 'OK':
                            warnings.warn("msg %s not marked\n"%emailid)
                        else:
                            if  conf.send_answer:
                                self.send_mail(my_mail['from'], response_text, response_subject, smtp, self.user, self.pwd)
        self.m.close()
        
    def send_mail(self, to, text, subject, smtp, user, pwd):
        def containsnonasciicharacters(st):
            return not all(ord(c) < 128 for c in st)   

        def addheader(message, headername, headervalue):
            if containsnonasciicharacters(headervalue):
                h = Header(headervalue, 'utf-8')
                message[headername] = h
            else:
                message[headername] = headervalue    
            return message
        
        
        try:
            msg = MIMEText(text.encode('utf-8'), 'html')
            msg = addheader(msg, 'Subject', subject) 
            msg = addheader(msg, 'From', user)
            msg = addheader(msg, 'To', to)
            msg['Content-Type'] = "text/html; charset=utf-8"
            conn = SMTP(smtp)
            conn.set_debuglevel(False)
            conn.login(user, pwd)
            try:
                conn.sendmail(user, to, msg.as_string())
            finally:
                conn.close()
                print "Warning: answer is sent to %s"%to
            

            
        except:
            warnings.warn("Response did not send to %s \n"%to)
            
        

  
           
init()    
db = Db()
accounts = db.select('accounts')

if  conf.send_answer: 
    settings = db.select('settings')
    (Id, response_text, response_subject) = settings[0]
else:
        response_text = None
        response_subject =None
        
for account in accounts:
    check_execution_timeout()
    Id, user, pwd, imap, smtp = account
    print "[%s] Check <%s>"%(datetime.datetime.now(), user)    
    p =Parser(user, pwd, imap, conf.imap_server_connection_timeout)
    p.get_mails(db, conf.imap_msg_skip_timeout, response_text, response_subject, smtp)
    print "Done\n\n"
db.close() 

    
